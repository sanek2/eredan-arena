#ifndef EREDANARENACABINET_H
#define EREDANARENACABINET_H

#include <QObject>
#include <QNetworkReply>
#include <QUrl>
#include <QMainWindow>
#include <QVector>
#include "singleton.h"
#include "eredanarenacards.h"
#include "eredanarenacard.h"
#include "eredanarenaplayer.h"


class EredanArenaCabinet : public QObject
{
    Q_OBJECT
public:

    enum RequestType {
        Unknown,
        Login,
        IsLogined,
        LoadAccountInfo,
        OpenKey,
        CollectCard,
        ConvertToXp,
        EndGame,
        LoadOnLoadWelcome
    };



    explicit EredanArenaCabinet(QObject *parent = 0);
    ~EredanArenaCabinet();
    void login(const QString& login, const QString& password);
    void isLogined();
    void loadAccountInfo();
    void collectCard();
    void convertToXp();
    void openKey();
    void endGame(const QList<int>& cardIds, bool isSurvival = false);
    void loadOnLoadWelcome(int loadedCardsSize = 0);
    void logout();

    EredanArenaPlayer *getPlayer() const;
    void setPlayer(EredanArenaPlayer *value);

signals:
    void signalOnLogin(bool successful);
    void signalIsLogined(bool successful);
    void signalAccountInfoLoaded();
    void signalOpenKey(bool successful, QString rewardType, QString rewardId, int status);
    void signalError(QString message);
    void signalUpdateDataReadProgress(qint64, qint64);
    void signalConvertToXp();
    void signalCollectCard();

private slots:
    //void httpFinished();
    void finishedRequestSlot(QNetworkReply *reply);
    void updateDataReadProgressSlot(qint64 current, qint64 total);

private:
    void handleRequest(const QByteArray& data, int type);

    EredanArenaPlayer* player;
    QNetworkAccessManager qnam;
};

#endif // EREDANARENACABINET_H
