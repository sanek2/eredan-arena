#include <QByteArray>
#include <QList>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QSharedPointer>
#include "singleton.h"
#include "eredanarenacabinet.h"
#include "mainwindow.h"
#include "eredanarenacard.h"
#include "eredanarenacards.h"
#include "eredanarenatexts.h"

EredanArenaCabinet::EredanArenaCabinet(QObject *parent) : QObject(parent)
{
    qnam.setCookieJar(new QNetworkCookieJar);
    connect(&qnam, SIGNAL(finished(QNetworkReply*)), this, SLOT(finishedRequestSlot(QNetworkReply*)));

    player = new EredanArenaPlayer(this);
}

EredanArenaCabinet::~EredanArenaCabinet()
{
    player->deleteLater();
}

void EredanArenaCabinet::login(const QString& login, const QString& password)
{
    qDebug() << "EredanArenaCabinet::login";
    QUrl url = "http://www.eredan-arena.com/";
    QUrlQuery postData;
    postData.addQueryItem("pseudo", login);
    postData.addQueryItem("pass", password);
    postData.addQueryItem("x", "0");
    postData.addQueryItem("y", "0");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    //QSharedPointer<QNetworkReply> reply = QSharedPointer<QNetworkReply>(new QNetworkReply, &QObject::deleteLater);
    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::Login);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);

}


void EredanArenaCabinet::isLogined()
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "Game_tick");
    postData.addQueryItem("params", "[]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::IsLogined);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);
}

void EredanArenaCabinet::logout()
{
    qnam.setCookieJar(new QNetworkCookieJar);
    player->clearPlayerCards();
}

void EredanArenaCabinet::loadAccountInfo()
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "Load_get_datas");
    postData.addQueryItem("params", "[]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::LoadAccountInfo);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);
}

void EredanArenaCabinet::collectCard()
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "BoutiqueGateway_boosterRecupererCarte");
    postData.addQueryItem("params", "[]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::CollectCard);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);
}

void EredanArenaCabinet::convertToXp()
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "BoutiqueGateway_boosterConvertirXP");
    postData.addQueryItem("params", "[]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::ConvertToXp);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);
}

void EredanArenaCabinet::openKey()
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "Coffres_getCoffre");
    postData.addQueryItem("params", "[]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::OpenKey);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);

}

void EredanArenaCabinet::endGame(const QList<int> &cardIds, bool isSurvival)
{

    QString sCardIds;

    if(cardIds.empty() == false)
        sCardIds = QString::number(cardIds[0]);

    for(int i = 1; i < cardIds.size(); i++)
    {
        sCardIds += "," + QString::number(cardIds[i]);
    }

    QString sIsSurvival = isSurvival ? "true" : "false";
    QString params = "[["+ sCardIds +"]," + sIsSurvival + "]";
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "Game_endGame");
    postData.addQueryItem("params", params);

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::OpenKey);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);

}

void EredanArenaCabinet::loadOnLoadWelcome(int loadedCardsSize)
{
    QUrl url = "http://www.eredan-arena.com/gateway.php?lang=en";
    QUrlQuery postData;
    postData.addQueryItem("device", "COMPUTER_WIN_DEVICE");
    postData.addQueryItem("version", "web");
    postData.addQueryItem("action", "Load_on_load_welcome");
    postData.addQueryItem("params", "[" + QString::number(loadedCardsSize) + ",null]");

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::LoadOnLoadWelcome);
    connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);

}

void EredanArenaCabinet::finishedRequestSlot(QNetworkReply *reply)
{
    if (reply->error())
    {
        emit signalError("HTTP error. Code: " + reply->error());
    }
    else
    {
        QList<QNetworkCookie> cookies = qvariant_cast<QList<QNetworkCookie>>(reply->header(QNetworkRequest::SetCookieHeader));

        if(cookies.count() != 0)
        {
            //you must tell which cookie goes with which url
            qnam.cookieJar()->setCookiesFromUrl(cookies, reply->url());
        }

        QByteArray data = reply->readAll();
        handleRequest(data, reply->property("requestType").toInt());
    }

    reply->deleteLater();
}

void EredanArenaCabinet::updateDataReadProgressSlot(qint64 current, qint64 total)
{
    qDebug() << current << " / " << total;
    emit signalUpdateDataReadProgress(current, total);
}

void EredanArenaCabinet::handleRequest(const QByteArray& data, int type)
{
    QJsonDocument jsonDoc;
    QJsonParseError jsonParseError;
    QString jsonStr;
    QJsonObject jsonObject;
    QJsonArray jsonArray;

    // onOpenKey
    QString rewardType;
    QString rewardValue;
    int rewardStatus = -1;
    int boosterStatus = -1;
    int boosterGain = 0;
    QString boosterCardId;
    QString playerCoin;

    switch(type)
    {
    case RequestType::Login:
        emit signalOnLogin(true);
        break;

    case RequestType::IsLogined:
        jsonStr = data;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else if(jsonObject["fatal"].toBool() == true)
        {
            emit signalIsLogined(false);
            qDebug() << "session_lost";
        }
        else
        {
            qDebug() << "auth_succsess";
            emit signalIsLogined(true);
        }

        break;

    case RequestType::OpenKey:
        jsonStr = data;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();


        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else if(jsonObject["coffre_infos"].isUndefined() == false && jsonObject["coffre_infos"].isNull() == false)
        {
            QJsonObject jsonCoffreInfo = jsonObject["coffre_infos"].toObject();

            if(jsonCoffreInfo["success"].toBool() == true)
            {
                if(jsonCoffreInfo["reward"].isUndefined() == false && jsonCoffreInfo["reward"].isNull() == false)
                {
                    QJsonObject jsonReward = jsonCoffreInfo["reward"].toObject();


                    rewardType = jsonReward["type"].toString();

                    if(jsonReward["status"].isUndefined() == false && jsonReward["status"].isNull() == false)
                        rewardStatus = jsonReward["status"].toInt();

                    if(rewardType == "cash")
                    {
                       int iRewardValue = jsonReward["value"].toInt();
                       rewardValue = QString::number(iRewardValue);

                       emit signalOpenKey(true, rewardType, rewardValue, rewardStatus);
                    }
                    else if(rewardType == "card")
                    {
                        rewardValue = jsonReward["value"].toString();

                        if(jsonReward["boosterInfos"].isUndefined() == false && jsonReward["boosterInfos"].isNull() == false)
                        {
                            QJsonObject jsonBootsterInfo = jsonReward["boosterInfos"].toObject();

                            if(jsonBootsterInfo["success"].toBool() == true)
                            {
                                if(jsonBootsterInfo["cash"].isUndefined() == false && jsonBootsterInfo["cash"].isNull() == false)
                                    player->setPlayerCash(jsonBootsterInfo["cash"].toVariant().toInt());

                                if(jsonBootsterInfo["coin"].isUndefined() == false && jsonBootsterInfo["coin"].isNull() == false)
                                    player->setPlayerCrystal(jsonBootsterInfo["coin"].toVariant().toInt());

                                boosterStatus = jsonBootsterInfo["status"].toInt();
                                boosterGain = jsonBootsterInfo["gain"].toInt();
                            }
                        }

                        emit signalOpenKey(true, rewardType, rewardValue, rewardStatus);
                    }
                    else if(rewardType == "booster")
                    {
                        rewardValue = QString::number(jsonReward["value"].toInt());

                        //
                        if(jsonReward["boosterInfos"].isUndefined() == false && jsonReward["boosterInfos"].isNull() == false)
                        {
                            QJsonObject jsonBootsterInfo = jsonReward["boosterInfos"].toObject();

                            if(jsonBootsterInfo["success"].toBool() == true)
                            {
                                if(jsonBootsterInfo["cash"].isUndefined() == false && jsonBootsterInfo["cash"].isNull() == false)
                                    player->setPlayerCash(jsonBootsterInfo["cash"].toVariant().toInt());

                                if(jsonBootsterInfo["coin"].isUndefined() == false && jsonBootsterInfo["coin"].isNull() == false)
                                    player->setPlayerCrystal(jsonBootsterInfo["coin"].toVariant().toInt());

                                boosterStatus = jsonBootsterInfo["status"].toInt();
                                boosterGain = jsonBootsterInfo["gain"].toInt();
                                boosterCardId = jsonBootsterInfo["id_carte"].toString();

                                emit signalOpenKey(true, rewardType, boosterCardId, boosterStatus);
                            }
                        }
                    }
                    else
                    {
                        emit signalOpenKey(true, rewardType, rewardValue, rewardStatus);
                    }
                }
            }
        }
        else if(jsonObject["result"].isUndefined() == false && jsonObject["result"].isNull() == false && jsonObject["result"].toBool() == false)
        {
            // no energy
            emit signalOpenKey(false, "", "", -1);
        }

        if(jsonObject["energy_infos"].isUndefined() == false && jsonObject["energy_infos"].isNull() == false)
        {
            QJsonObject jsonKeyInfo = jsonObject["energy_infos"].toObject();
            player->parseJsonEnergyInfo(jsonKeyInfo);

            emit signalAccountInfoLoaded();
        }

        break;

    case RequestType::LoadAccountInfo:
        jsonStr = data;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else
        {
            if(jsonObject["textes"].isUndefined() == false && jsonObject["textes"].isNull() == false)
            {
                Singleton<EredanArenaTexts>::instance().parseJsonTexts(jsonObject["textes"].toObject());
            }

            if(jsonObject["player"].isUndefined() == false && jsonObject["player"].isNull() == false)
            {
                QJsonObject jsonPlayer = jsonObject["player"].toObject();
                player->parseJsonPlayerInfo(jsonObject["player"].toObject());

            }

            if(jsonObject["cartes"].isUndefined() == false && jsonObject["cartes"].isNull() == false)
            {
                QJsonObject jsonCards = jsonObject["cartes"].toObject();

                Singleton<EredanArenaCards>::instance().parseJsonCards(jsonCards);
            }

            if(jsonObject["cartes_player"].isUndefined() == false && jsonObject["cartes_player"].isNull() == false)
            {
                QJsonObject jsonCards = jsonObject["cartes_player"].toObject();

                player->parseJsonPlayerCards(jsonCards);
            }
        }

        emit signalAccountInfoLoaded();

        break;

    case RequestType::CollectCard:
        signalCollectCard();
        break;

    case RequestType::ConvertToXp:
        signalConvertToXp();
        break;

    case RequestType::LoadOnLoadWelcome:
        jsonStr = data;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else
        {
            if(jsonObject["cartes"].isUndefined() == false && jsonObject["cartes"].isNull() == false)
            {
                QJsonObject jsonCards = jsonObject["cartes"].toObject();

                Singleton<EredanArenaCards>::instance().parseJsonCards(jsonCards);
            }
        }
        break;

    default:
        break;
    }
}

EredanArenaPlayer *EredanArenaCabinet::getPlayer() const
{
    return player;
}

void EredanArenaCabinet::setPlayer(EredanArenaPlayer *value)
{
    player = value;
}

