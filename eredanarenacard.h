#ifndef EREDANARENACARD_H
#define EREDANARENACARD_H

#include <QObject>
#include <QList>
#include <QJsonObject>
#include "eredanarenacardskill.h"

class EredanArenaCard : public QObject
{
    Q_OBJECT
public:
    explicit EredanArenaCard(QObject *parent = 0);

    void parseJsonCardInfo(QJsonObject& jsonCard);
    void parseJsonPlayerCardInfo(QJsonObject& jsonPlayerCard);

    bool getActive() const;
    void setActive(bool value);

    int getId() const;
    void setId(int value);

    int getIdCard() const;
    void setIdCard(int value);

    int getIdPlayer() const;
    void setIdPlayer(int value);

    int getXp() const;
    void setXp(int value);

    int getTypeCard() const;
    void setTypeCard(int value);

    int getExclusive() const;
    void setExclusive(int value);

    int getLevel() const;
    void setLevel(int value);

    int getIdTextName() const;
    void setIdTextName(int value);

    QString getName();

    QList<EredanArenaCardSkill *> getCardSkills() const;
    void setCardSkills(const QList<EredanArenaCardSkill *> &value);

signals:

public slots:

private:
    // player info
    bool active = false;
    int id = 0;
    int idCard = 0;
    int idPlayer = 0; // joueur
    int xp = 0;
    int typeCard = 0;
    int idTextName = 0;

    // card info
    int exclusive = 0;
    int level = 0;

    QList<EredanArenaCardSkill *> cardSkills;

};

#endif // EREDANARENACARD_H
