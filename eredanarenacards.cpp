#include <QObject>
#include <QJsonObject>
#include <QJsonValue>
#include <QVariant>
#include "eredanarenacard.h"
#include "eredanarenacards.h"

EredanArenaCards::EredanArenaCards(QObject *parent) : QObject(parent)
{
    for(int i = 0; i < MAX_CARD_ID; i++)
    {
        cards[i] = 0;
    }
}

EredanArenaCards::~EredanArenaCards()
{
    clear();
}

EredanArenaCard* EredanArenaCards::getCard(int id)
{
    if(id < MAX_CARD_ID && id >= 0)
    {
        if(cards[id] == 0)
            cards[id] = new EredanArenaCard(this);

        return cards[id];
    }
    else
    {
        emit signalError("Parse card error. Card id > " + QString::number(MAX_CARD_ID) + ". " + "Card id: " + id + ". ");

        return new EredanArenaCard(this);
    }

}

int EredanArenaCards::getLoadedCardsSize()
{
    int resSize = 0;

    for(int i = 0; i < MAX_CARD_ID; i++)
    {
        if(cards[i])
        {
            if(cards[i]->getIdCard())
                resSize ++;
        }
    }

    return resSize;
}

bool EredanArenaCards::issetCard(int id)
{
    if(id < MAX_CARD_ID && id >= 0)
    {
        return cards[id] != 0;
    }

    return false;
}

void EredanArenaCards::parseJsonCards(const QJsonObject& jsonCards)
{
    foreach (const QJsonValue& jsonCard, jsonCards)
    {
        QJsonObject jsonObjectCard = jsonCard.toObject();

        int cardId = jsonObjectCard["id"].toVariant().toInt();

        EredanArenaCard* card = getCard(cardId);

        card->parseJsonCardInfo(jsonObjectCard);
    }
}

void EredanArenaCards::clear()
{
    for(int i = 0; i < MAX_CARD_ID; i++)
    {
        if(cards[i])
        {
            cards[i]->deleteLater();
            cards[i] = 0;
        }
    }
}
