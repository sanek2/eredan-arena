#ifndef PROGRAMSITE_H
#define PROGRAMSITE_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "qblowfish.h"

class ProgramSite : public QObject
{
    Q_OBJECT
public:

    enum RequestType
    {
        CheckLicensy,
        ActivateLicensyTrial,
        CheckUpdate
    };

    explicit ProgramSite(QObject *parent = 0);
    ~ProgramSite();

    void checkLicensy(const QString& licensyKey);
    void activateLicensyTrial(const QString& accountName);
    QString getUrlSite() const;


signals:
    void signalError(const QString& error);
    void signalLCR(const QString& accountName, int currentTime, int licensyEndTime);    // Licensy check request result
    void signalTrialKeyRecieved(const QString& key);

public slots:

private slots:
    void finishedRequestSlot(QNetworkReply *reply);
    void onLCR();


private:
    const QString urlSite = "http://127.0.0.1/eredan-arena/";
    QNetworkAccessManager qnam;
    QString accountName;
    int licensyEndTime = 0;
    int currentTime = 0;
    QBlowfish *blowfish;

    void handleRequest(const QByteArray& data, int type);
};

#endif // PROGRAMSITE_H
