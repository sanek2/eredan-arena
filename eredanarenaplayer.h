#ifndef EREDANARENAPLAYER_H
#define EREDANARENAPLAYER_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QVariant>
#include <QMap>
#include <QJsonObject>
#include "singleton.h"
#include "eredanarenacards.h"
#include "eredanarenacard.h"

class EredanArenaPlayer : public QObject
{
    Q_OBJECT
public:
    explicit EredanArenaPlayer(QObject *parent = 0);

    void parseJsonPlayerInfo(const QJsonObject& jsonPlayer);

    int getPlayerId() const;
    void setPlayerId(int value);

    int getPlayerCash() const;
    void setPlayerCash(int value);

    int getPlayerCrystal() const;
    void setPlayerCrystal(int value);

    QString getPlayerName() const;
    void setPlayerName(const QString &value);

    QString getPlayerToken() const;
    void setPlayerToken(const QString &value);

    int getPlayerKeyNum() const;
    void setPlayerKeyNum(int value);

    int getPlayerKeyMax() const;
    void setPlayerKeyMax(int value);

    int getPlayerKeyRegen() const;
    void setPlayerKeyRegen(int value);

    int getPlayerLevel() const;
    void setPlayerLevel(int value);

    int getPlayerXp() const;
    void setPlayerXp(int value);

    int getPlayerXpMax() const;
    void setPlayerXpMax(int value);

    int getPlayerXpMin() const;
    void setPlayerXpMin(int value);

    EredanArenaCard* getPlayerCard(int id);
    const QMap <int, EredanArenaCard*>& getPlayerCards();
    EredanArenaCard *loadPlayerCardByCard(const EredanArenaCard* card);
    void deletePlayerCard(int id);
    void clearPlayerCards();
    void parseJsonPlayerCards(const QJsonObject& jsonCards);

    void parseJsonEnergyInfo(const QJsonObject &jsonKeyInfo);

signals:
    void signalError(QString message);
    void signalPlayerCardsUpdated();

public slots:

private:

    int playerId;
    int playerCash;
    int playerCrystal;
    QString playerName;
    QString playerToken;

    int playerKeyNum;
    int playerKeyMax;
    int playerKeyRegen;

    int playerLevel;
    int playerXp;
    int playerXpMax;
    int playerXpMin;

    QMap <int, EredanArenaCard*> playerCards;
};

#endif // EREDANARENAPLAYER_H
