#include <QObject>
#include <QTcpSocket>
#include <QDataStream>
#include <QString>
#include <QByteArray>
#include <QDebug>
#include <QtXml>
#include "QtZlib/zconf.h"
#include "QtZlib/zlib.h"
#include "eredanarenabattle.h"


EredanArenaBattle::EredanArenaBattle(QObject *parent) : QObject(parent)
{
    tcpSocket = new QTcpSocket(this);
    connect(tcpSocket, &QTcpSocket::readyRead, this, &EredanArenaBattle::readTcpData);
    //connect(tcpSocket, &QTcpSocket::error, this, &EredanArenaBattle::tcpErrorSlot);
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tcpErrorSlot(QAbstractSocket::SocketError)));

    lastUserDiceList = QList <int>() << -1 << -1 << -1 << -1 << -1 << -1;
}

EredanArenaBattle::~EredanArenaBattle()
{
    tcpSocket->close();
    tcpSocket->deleteLater();
}

void EredanArenaBattle::start(int type, QString token)
{
    isPve = 0;
    roundNum = 0;

    if(type != Type::Undefined)
        setType(type);

    if(token.length())
        setToken(token);

    tcpSocket->connectToHost("195.60.188.25", 9339);

    if (tcpSocket->waitForConnected(5000))
    {
        handleSocketConnection();
    }

}

int EredanArenaBattle::getType() const
{
    return type;
}

void EredanArenaBattle::setType(int value)
{
    type = value;
}

QString EredanArenaBattle::getToken() const
{
    return token;
}

void EredanArenaBattle::setToken(const QString &value)
{
    token = value;
}

void EredanArenaBattle::readTcpData()
{

    if(tcpSocket->bytesAvailable())
    {
        QByteArray data = tcpSocket->readAll();
        buffer.append(data);

        if(buffer.indexOf((char)0) != -1)
        {
            QList<QByteArray> packets = buffer.split(0);

            buffer = packets.last();

            for(int i = 0; i < packets.size() - 1; i++)
            {
                //qDebug() << packets[i];
                handleMessage(packets[i]);
            }
        }
    }
}

void EredanArenaBattle::handleSocketConnection()
{
    QByteArray msg = "<msg t='sys'><body action='verChk' r='0'><ver v='161' /></body></msg>";
    tcpSocket->write(msg.constData(), msg.length() + 1);
}

void EredanArenaBattle::handleMessage(const QByteArray& sPacket)
{
    if (sPacket.size() > 0)
    {
        if (sPacket[0] == '<')
        {
            xmlReceived(sPacket);
        }
        else if (sPacket[0] == '%')
        {
            strReceived(sPacket);
        }
        else if (sPacket[0] == '{')
        {
            jsonReceived(sPacket);
        }
    }

}

void EredanArenaBattle::jsonReceived(const QByteArray& sPacket)
{
    QJsonDocument jsonDoc;
    QJsonParseError jsonParseError;
    QString jsonStr;
    QJsonObject jsonObject;
    QJsonArray jsonArray;

    jsonStr = sPacket;

    jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
    jsonObject = jsonDoc.object();

    //qDebug() << jsonStr;
    //qDebug() << jsonObject;

    if(jsonStr.length() == 0)
    {
        signalError("Empty answer from server.");
    }
    else if (jsonParseError.error != QJsonParseError::NoError)
    {
        QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
        signalError(err);
    }
    else
    {
        QString messageType = jsonObject["t"].toVariant().toString();

        if (messageType == "xt")
        {
            handleMessageXt(jsonObject["b"].toObject());
        }

        if (messageType == "sys")
        {
            //handleMessageSys(jsonObject["b"].toObject());
            qDebug() << "Warning undefined packet sys json.";
        }
    }
}

QByteArray gUncompress(const QByteArray &data)
{
    if (data.size() <= 4) {
        qWarning("gUncompress: Input data is truncated");
        return QByteArray();
    }

    QByteArray result;

    int ret;
    z_stream strm;
    static const int CHUNK_SIZE = 1024;
    char out[CHUNK_SIZE];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = data.size();
    strm.next_in = (Bytef*)(data.data());

    ret = inflateInit2(&strm, 15 +  32); // gzip decoding
    if (ret != Z_OK)
        return QByteArray();

    // run inflate()
    do {
        strm.avail_out = CHUNK_SIZE;
        strm.next_out = (Bytef*)(out);

        ret = inflate(&strm, Z_NO_FLUSH);
        Q_ASSERT(ret != Z_STREAM_ERROR);  // state not clobbered

        switch (ret) {
        case Z_NEED_DICT:
            ret = Z_DATA_ERROR;     // and fall through
        case Z_DATA_ERROR:
        case Z_MEM_ERROR:
            (void)inflateEnd(&strm);
            return QByteArray();
        }

        result.append(out, CHUNK_SIZE - strm.avail_out);
    } while (strm.avail_out == 0);

    // clean up and return
    inflateEnd(&strm);
    return result;
}

void EredanArenaBattle::handleMessageXt(const QJsonObject& jsonObjectParam1)
{
    QJsonDocument jsonDoc;
    QJsonParseError jsonParseError;
    QString jsonStr;
    QJsonObject jsonObject;
    QJsonArray jsonArray;

    QJsonObject _loc9_dataObj = jsonObjectParam1["o"].toObject();

    // onExtensionResponse
    QJsonObject _loc8_ = _loc9_dataObj;
    QString _loc16_ = _loc8_["_cmd"].toVariant().toString();

    if (_loc8_["deflated"].isNull() == false && _loc8_["deflated"].isUndefined() == false && _loc8_["deflated"].toBool() == true)
    {
        QByteArray compressedStr = _loc8_["compressed_data"].toVariant().toByteArray();
        QByteArray base64DecodedStr = QByteArray::fromBase64(compressedStr);
        QByteArray decompressedStr = gUncompress(base64DecodedStr);

        QByteArray _loc4_ = decompressedStr;



        jsonStr = _loc4_;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else
        {
            onExtensionResponse(_loc8_, jsonObject, _loc16_);
        }
    }
    else
    {
        onExtensionResponse(_loc8_, _loc8_, _loc16_);
    }

}

void EredanArenaBattle::onExtensionResponse(const QJsonObject& _loc8_, const QJsonObject& _loc15_, QString _loc16_)
{
    QString _loc25_ = _loc16_;

    qDebug() << "onExtensionResponse: "  << _loc25_;

    if ("fkInitAdvancedExtension" != _loc25_)
    {
        if ("fkStopAdvancedExtension" != _loc25_)
        {
            if ("userVars" != _loc25_)
            {
                if ("userCount" != _loc25_)
                {
                    if ("blueBoxed" != _loc25_)
                    {
                        if ("roomDestroyed" != _loc25_)
                        {
                            if (true)
                            {
                                if (_loc16_ == "logOK")
                                {

                                    //onLogin(_loc15_["name"].string_value(), _loc15_["id"].int_value(), true);

                                    if (_loc15_["zone"].isUndefined() == false && _loc15_["zone"].isNull() == false)
                                    {
                                        //onLogin(_loc15_["name"].string_value(), _loc15_["id"].int_value(), true, _loc15_["zone"].string_value());

                                    }

                                    return;
                                }

                                if (_loc16_ == "logKO")
                                {
                                    //onLogin("", 0, false);

                                    return;
                                }
                            }

                            int _loc10_ = 0;
                            int _loc22_ = 0;

                             if (_loc8_["fkAdvMsgId"].isUndefined() == false && _loc8_["fkAdvMsgId"].isNull() == false)
                            {
                                 QJsonObject t = _loc8_["fkAdvMsgId"].toObject();
                                _loc10_ = t["room"].toVariant().toInt();
                                _loc22_ = t["id"].toVariant().toInt();
                            }

                             dispatchCommandToDelegates(_loc16_, _loc15_);
                        }
                    }
                }
            }
        }
    }
}

void EredanArenaBattle::dispatchCommandToDelegates(const QString& param2, const QJsonObject& param3)
{
    if (param2 == "startRound")
    {
        if(param3["anim_list"].isUndefined() == false && param3["anim_list"].isNull() == false)
        {
            for(auto &animListChild : param3["anim_list"].toArray())
            {
                for(auto &jsonAnim : animListChild.toArray())
                {
                    QJsonObject jsonAnimObject = jsonAnim.toObject();

                    int animId = jsonAnimObject["id"].toVariant().toInt();

                    qDebug() << "animId: " << animId;

                    switch (animId)
                    {
                    case AnimationList::resultRound: parseJsonResultRound(jsonAnimObject); break;
                    case AnimationList::resultMatch: parseJsonResultMatch(jsonAnimObject); break;
                    case AnimationList::beginFight: parseJsonBeginFight(jsonAnimObject); break;

                    default:
                        break;
                    }
                }
            }
        }

        // bug 100% win, uncomment

        //if (roundNum > 1)
        {
            //qDebug() << "attacker, sendPlayCard ";
            //sendPlayCard(0);
        }
    }

    if (param2 == "mactchLaunch")
    {
        if (param3["user_mode_fight"].isUndefined() == false && param3["user_mode_fight"].isNull() == false)
        {
            isAttacker = (int)(param3["user_mode_fight"].toVariant().toInt() == 0);
        }

        if (param3["user_bench"].isUndefined() == false && param3["user_bench"].isNull() == false)
        {
            userCardList.clear();
            qDebug() << "    - user bench";

            for (auto &card : param3["user_bench"].toArray())
            {
                QJsonObject jsonObjectCard = card.toObject();
                int cardId = jsonObjectCard["id_listing"].toVariant().toInt();
                userCardList.push_back(Singleton<EredanArenaCards>::instance().getCard(cardId));
                qDebug() << "    - " << cardId;
            }
        }

        if (param3["opp_bench"].isUndefined() == false && param3["opp_bench"].isNull() == false)
        {
            oppCardList.clear();
            qDebug() << "    - opponent bench";

            for (auto &card : param3["opp_bench"].toArray())
            {
                QJsonObject jsonObjectCard = card.toObject();
                int cardId = jsonObjectCard["id_listing"].toVariant().toInt();
                oppCardList.push_back(Singleton<EredanArenaCards>::instance().getCard(cardId));
                qDebug() << "    - " << cardId;
            }
        }

        if (param3["pve"].isUndefined() == false && param3["pve"].isNull() == false)
        {
            isPve = (param3["pve"].toVariant().toInt() == 1);
        }

        if (isAttacker)
        {
            //Sleep(3000);
            qDebug() << "attacker, sendPlayCard ";
            sendPlayCard(0);
            roundNum++;
        }
        else
            qDebug() << "not attacker";
    }

    if (param2 == "diceResult")
    {
        QList <int> diceResult;
        int nbRetry = 0;

        if (param3["dice_result"].isUndefined() == false && param3["dice_result"].isNull() == false)
        {
            for (auto &dice : param3["dice_result"].toArray())
            {
                diceResult.push_back(dice.toVariant().toInt());
            }
        }

        if(param3["nb_retry"].isUndefined() == false && param3["nb_retry"].isNull() == false)
        {
            nbRetry = param3["nb_retry"].toVariant().toInt();
        }

        onDiceResult(diceResult, nbRetry);

        //sendPlayDice(stream, 1, 1, 1, 1, 1, 1);
    }

    if (param2 == "choiceCardDef")
    {
        int selectedCardNum = param3["opp_card_active"].toVariant().toInt();

        if(selectedCardNum >= 0 && selectedCardNum < oppCardList.size())
        {
            EredanArenaCard* selectedCard = oppCardList[selectedCardNum];
            emit signalOppChoiceCard(selectedCard);
        }
        else emit signalError("Selected card not in cardslist");

        sendPlayCard(0);
        roundNum++;
    }

    if (param2 == "choiceCard")
    {

        //QList<int> useDiceList = QList<int>() << 1 << 1 << 1 << 1 << 1 << 1;
        //sendValidDice(useDiceList);

        if(param3["user_card_active"].isUndefined() == false && param3["user_card_active"].isNull() == false)
        {
            int cardNum = param3["user_card_active"].toVariant().toInt();

            qDebug() << "signalUserSelectCard" << cardNum;
        }

        if(param3["opp_card_active"].isUndefined() == false && param3["opp_card_active"].isNull() == false)
        {
            int selectedCardNum = param3["opp_card_active"].toVariant().toInt();

            if(selectedCardNum >= 0 && selectedCardNum < oppCardList.size())
            {
                EredanArenaCard* selectedCard = oppCardList[selectedCardNum];
                emit signalOppChoiceCard(selectedCard);
            }
            else emit signalError("Selected card not in cardslist");
        }

        QList <int> diceResult;

        if (param3["dice_result"].isUndefined() == false && param3["dice_result"].isNull() == false)
        {
            for (auto &dice : param3["dice_result"].toArray())
            {
                diceResult.push_back(dice.toVariant().toInt());
            }
        }

        onDiceResult(diceResult, 3);
    }

    if (param2 == "displayCardFight")
    {
        isAttacker != isAttacker;
    }
}

void EredanArenaBattle::sendPlayCard(int cardNumber)
{

    QByteArray pve = (isPve ? "Pve" : "");

    {
        QByteArray msg = "%xt%GameLogic" + pve + "%fkAdvReclaimMsg%" + QByteArray::number(roomId) + "%5%";

        qDebug() << "send: " << msg;

        tcpSocket->write(msg.constData(), msg.length() + 1);
    }

    {

        QByteArray msg = "%xt%GameLogic" + pve + "%playCard%" + QByteArray::number(roomId) + "%" + QByteArray::number(cardNumber) + "%";

        qDebug() << "send: " << msg;

        tcpSocket->write(msg.constData(), msg.length() + 1);
    }

    if(cardNumber >= 0 && cardNumber < userCardList.size())
    {
        emit signalUserChoiceCard(userCardList[cardNumber]);
        lastUserCard = userCardList[cardNumber];
    }
    else emit signalError("Selected card not in cardslist");
}

void EredanArenaBattle::sendPlayDice(const QList<int> &useDiceList)
{
    if(useDiceList.empty() == false)
    {
        QByteArray pve = (isPve ? "Pve" : "");

        QByteArray msg = "%xt%GameLogic" + pve + "%playDice%" + QByteArray::number(roomId) + "%";

        for(int i = 0; i < useDiceList.size(); i++)
        {
            msg += QString::number(useDiceList[i]) + "%";
        }

        qDebug() << "send: " << msg;

        tcpSocket->write(msg.constData(), msg.length() + 1);

        emit signalPlayDice(useDiceList);
    }
}

void EredanArenaBattle::sendValidDice(const QList<int> &useDiceList)
{
    if(useDiceList.empty() == false)
    {
        QByteArray pve = (isPve ? "Pve" : "");

        QByteArray msg = "%xt%GameLogic" + pve + "%validDice%" + QByteArray::number(roomId) + "%";

        for(int i = 0; i < useDiceList.size(); i++)
        {
            msg += QString::number(useDiceList[i]) + "%";
        }

        qDebug() << "send: " << msg;

        tcpSocket->write(msg.constData(), msg.length() + 1);

        emit signalValidDice(useDiceList);
    }

}

void EredanArenaBattle::onDiceResult(const QList<int> &diceList, int numRetry)
{
    updateLastUserDiceList(diceList);
    qDebug() << "signalUserDiceResult onDiceResult" << lastUserDiceList;
    emit signalUserDiceResult(diceList);

    if(numRetry > 0)
    {
        if(lastUserCard)
        {
            QList<EredanArenaCardSkill *>lastUserCardSkillsList = lastUserCard->getCardSkills();
            int allowedActivations[10] = {0};

            foreach (EredanArenaCardSkill* cardSkill, lastUserCardSkillsList)
            {
                QList<int> activationList = cardSkill->getActivation();

                foreach(int activation, activationList)
                {
                    if(activation >= 0 && activation <= 3)
                    {
                        allowedActivations[activation] = 1;
                    }
                    else emit signalError("Activation should be >= 0 and <= 3");
                }
            }

            QList<int> useDiceList;

            foreach (int dice, lastUserDiceList)
            {
                if(dice >= 0 && dice <= 3)
                {
                    if(allowedActivations[dice] == 1)
                    {
                        useDiceList.push_back(1);
                    }
                    else
                    {
                        useDiceList.push_back(0);
                    }
                }
                else
                {
                    useDiceList.push_back(0);
                    emit signalError("Dice id should be >= 0 and <= 3");
                }
            }

            sendPlayDice(useDiceList);
        }
        else
        {
            QList<int> useDiceList = QList<int>() << 1 << 1 << 1 << 1 << 1 << 0;
            //sendValidDice(useDiceList);
            sendPlayDice(useDiceList);
        }
    }
    else
    {

    }
}

void EredanArenaBattle::parseJsonResultMatch(QJsonObject &jsonObject)
{
    int battleResult = jsonObject["value"].toVariant().toInt();
    int scoreUser = jsonObject["scoreUser"].toVariant().toInt();
    int scoreOpp = jsonObject["scoreOpp"].toVariant().toInt();
    QString endReason = jsonObject["endReason"].toVariant().toString();
    QString userName = jsonObject["pseudoUser"].toVariant().toString();
    QString oppName = jsonObject["pseudoOpp"].toVariant().toString();

    qDebug() << "ResultMatch: " << battleResult << scoreUser << scoreOpp;
    emit signalResultMatch(battleResult, endReason, userName, oppName, scoreUser, scoreOpp);

}

void EredanArenaBattle::parseJsonBeginFight(QJsonObject &jsonObject) // dice_result
{
    QList <int> userDiceList;
    QList <int> oppDiceList;

    QJsonObject jsonObjectArgs = jsonObject["args"].toObject();

    if(jsonObjectArgs["user_dice_res"].isUndefined() == false && jsonObjectArgs["user_dice_res"].isNull() == false)
    {
        foreach(auto &jsonDice, jsonObjectArgs["user_dice_res"].toArray())
        {
            int dice = jsonDice.toVariant().toInt();
            userDiceList.push_back(dice);
            //qDebug() << "userDice: " << dice;
        }

        updateLastUserDiceList(userDiceList);
        qDebug() << "signalUserDiceResult parseJsonBeginFight" << lastUserDiceList;
        emit signalUserDiceResult(userDiceList);
    }

    if(jsonObjectArgs["opp_dice_res"].isUndefined() == false && jsonObjectArgs["opp_dice_res"].isNull() == false)
    {
        foreach(auto &jsonDice, jsonObjectArgs["opp_dice_res"].toArray())
        {
            int dice = jsonDice.toVariant().toInt();
            oppDiceList.push_back(dice);
            //qDebug() << "oppDice: " << dice;
        }

        emit signalOppDiceResult(oppDiceList);
    }
}

void EredanArenaBattle::parseJsonResultRound(QJsonObject &jsonObject)
{
    int oppScore = jsonObject["scoreOpp"].toVariant().toInt();
    int userScore = jsonObject["scoreUser"].toVariant().toInt();
    int value = jsonObject["value"].toVariant().toInt();

    emit signalResultRound(value, userScore, oppScore);
}

void EredanArenaBattle::updateLastUserDiceList(const QList<int> &diceList)
{
    if(diceList.size() != 6)
    {
        emit signalError("Dice list size must be 6");
    }
    else
    {
        for(int i = 0; i < 6; i++)
        {
            if(diceList[i] != -1)
                lastUserDiceList[i] = diceList[i];
        }
    }
}

void EredanArenaBattle::strReceived(const QByteArray &sPacket)
{

    QList<QByteArray> parts = sPacket.split('%');
    QByteArray messageType;

    if(parts.isEmpty() == false)
        parts.pop_back();

    if(parts.isEmpty() == false)
        parts.pop_front();

    if(parts.isEmpty() == false)
        messageType = parts[0];

    if(parts.isEmpty() == false)
        parts.pop_front();

    if (messageType == "xt")
    {
        handleMessageXt(parts);
    }

    if (messageType == "sys")
    {
        //handleMessageSys(parts);
        qDebug() << "Warning undefined sys str message";
    }
}

void EredanArenaBattle::handleMessageXt(QList<QByteArray>& parts)
{
    QByteArray xtMessageType;

    if(parts.isEmpty() == false)
        xtMessageType = parts[0];

    if(parts.isEmpty() == false)
        parts.pop_front();

    if(parts.isEmpty() == false)
        parts.pop_front();


    if (xtMessageType == "roomDestroyed")
    {
        tcpSocket->close();
    }

}

void EredanArenaBattle::xmlReceived(const QByteArray& sPacket)
{
    QDomDocument doc;
    doc.setContent(sPacket);
    QDomElement messageElement = doc.firstChildElement();
    QString messageType = messageElement.attribute("t");

    qDebug() << messageType;

    if(messageType == "xt")
    {
        //handleMessageXt(doc);
        qDebug() << "Warning undefined packet xt xml.";
    }

    if(messageType == "sys")
    {
        handleMessageSys(messageElement);
    }

}

void EredanArenaBattle::handleMessageSys(const QDomElement& messageElement)
{
    QString action = messageElement.namedItem("body").toElement().attribute("action");
    QString r = messageElement.namedItem("body").toElement().attribute("r");

    qDebug() << "sysMsg: " << action;

    if (action == "apiOK")
    {
        QByteArray msg = "<msg t='sys'><body action='rndK' r='-1'></body></msg>";
        qDebug() << "send: " << msg;
        tcpSocket->write(msg.constData(), msg.length() + 1);
    }

    if (action == "rndK")
    {
        QByteArray msg = "<msg t='sys'><body action='login' r='0'><login z='EredanBattleLobby'><nick><![CDATA[]]></nick><pword><![CDATA[]]></pword></login></body></msg>";
        qDebug() << "send: " << msg;
        tcpSocket->write(msg.constData(), msg.length() + 1);
    }

    if (action == "logOK")
    {
        QByteArray msg = "<msg t='sys'><body action='autoJoin' r='-1'></body></msg>";
        qDebug() << "send: " << msg;
        tcpSocket->write(msg.constData(), msg.length() + 1);
    }

    if (action == "joinOK")
    {
        if (r == "2")
        {
            QByteArray msg = "%xt%EredanBattleLobby%lobby%2%" + getToken().toUtf8() + "%training%1473655083%";
            //QByteArray msg = "%xt%EredanBattleLobby%lobby%2%57C4BEED3F91257C4BEED3F94A%defi%0%395493%";
            //QByteArray msg = "%xt%EredanBattleLobby%lobby%2%57C4BEED3F91257C4BEED3F94A%survival%1188689626%";	// cot
            //QByteArray msg = "%xt%EredanBattleLobby%lobby%2%57BB39B75C3D557BB39B75C40F%survival%1191567824%";	// lalko
            //QByteArray msg = "%xt%EredanBattleLobby%lobby%2%57BB39B75C3D557BB39B75C40F%training%1473655083%";		// lalko

            qDebug() << "send: " << msg;
            tcpSocket->write(msg.constData(), msg.length() + 1);
        }
    }

    if (action == "rmList")
    {
        setRoomId(messageElement.namedItem("body").namedItem("rmList").namedItem("rm").toElement().attribute("id").toInt());
    }

    if (action == "logout")
    {

    }

}

void EredanArenaBattle::tcpErrorSlot(QAbstractSocket::SocketError error)
{
    qDebug() << "Tcp error: " << error;
}

QTcpSocket *EredanArenaBattle::getTcpSocket() const
{
    return tcpSocket;
}

void EredanArenaBattle::setTcpSocket(QTcpSocket *value)
{
    tcpSocket = value;
}

int EredanArenaBattle::getIsAttacker() const
{
    return isAttacker;
}

void EredanArenaBattle::setIsAttacker(int value)
{
    isAttacker = value;
}

int EredanArenaBattle::getIsPve() const
{
    return isPve;
}

void EredanArenaBattle::setIsPve(int value)
{
    isPve = value;
}

int EredanArenaBattle::getRoundNum() const
{
    return roundNum;
}

void EredanArenaBattle::setRoundNum(int value)
{
    roundNum = value;
}

int EredanArenaBattle::getRoomId() const
{
    return roomId;
}

void EredanArenaBattle::setRoomId(int value)
{
    roomId = value;
}

