#ifndef EREDANARENACARDSKILL_H
#define EREDANARENACARDSKILL_H

#include <QObject>
#include <QList>
#include <QString>
#include <QJsonObject>


class EredanArenaCardSkill : public QObject
{
    Q_OBJECT
public:
    explicit EredanArenaCardSkill(QObject *parent = 0);

    void parseJsonCardSkill(const QJsonObject& jsonObjectSkill);

    int getEffectId() const;
    void setEffectId(int value);

    int getId() const;
    void setId(int value);

    int getCardId() const;
    void setCardId(int value);

    int getValue() const;
    void setValue(int value);

    QList<int> getActivation() const;
    void setActivation(const QList<int> &value);

    int getIdEffectName() const;
    void setIdEffectName(int value);

    int getIdEffectDescription() const;
    void setIdEffectDescription(int value);

signals:

public slots:

private:
    int id = 0;
    int effectId = 0;
    int cardId = 0;
    int val = 0;
    QList <int> activation;
    int idEffectName = 0;
    int idEffectDescription = 0;
};

#endif // EREDANARENACARDSKILL_H
