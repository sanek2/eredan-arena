#-------------------------------------------------
#
# Project created by QtCreator 2016-09-16T20:55:45
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = eredan_arena_cabinet_v2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
    eredanarenacard.cpp\
    eredanarenacabinet.cpp\
    eredanarenacards.cpp\
    eredanarenatexts.cpp\
    eredanarenabattle.cpp \
    eredanarenaplayer.cpp \
    eredanarenacardskill.cpp \
    programsite.cpp \
    programconfigs.cpp \
    qblowfish.cpp

HEADERS  += mainwindow.h\
    eredanarenacard.h\
    eredanarenacabinet.h\
    call_once.h \
    singleton.h \
    eredanarenacards.h \
    eredanarenatexts.h \
    eredanarenabattle.h \
    eredanarenaplayer.h \
    eredanarenacardskill.h \
    programsite.h \
    programconfigs.h \
    qblowfish.h \
    qblowfish_p.h

FORMS    += mainwindow.ui
