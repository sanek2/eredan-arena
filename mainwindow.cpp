#include <QtNetwork>
#include <QDateTime>
#include <QTime>
#include <QTimer>
#include <QTcpSocket>
#include <QDesktopServices>
#include <QMessageBox>
#include <qblowfish.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "singleton.h"
#include "eredanarenacards.h"
#include "eredanarenatexts.h"
#include "programconfigs.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentWidget(ui->stackedWidgetPageAccount);
    ui->tabWidget->setCurrentWidget(ui->tabWidgetPageAccount);
    ui->stackedWidgetLicensy->setCurrentWidget(ui->stackedWidgetPageLicensyUnregistered);

    configs = new ProgramConfigs(this);
    ui->lineEditLicensyKey->setText(configs->getLicensyKey());

    cabinet = new EredanArenaCabinet(this);
    connect(cabinet, &EredanArenaCabinet::signalOnLogin, this, &MainWindow::onCabinetLoginSlot);
    connect(cabinet, &EredanArenaCabinet::signalIsLogined, this, &MainWindow::onIsCabinetLoginedSlot);
    connect(cabinet, &EredanArenaCabinet::signalAccountInfoLoaded, this, &MainWindow::onCabinetAccountInfoLoadedSlot);
    connect(cabinet, &EredanArenaCabinet::signalUpdateDataReadProgress, this, &MainWindow::onUpdateDataReadProgressSlot);
    connect(cabinet, &EredanArenaCabinet::signalOpenKey, this, &MainWindow::onCabinetOpenKeySlot);

    connect(cabinet->getPlayer(), &EredanArenaPlayer::signalPlayerCardsUpdated, this, &MainWindow::onPlayerCardsUpdatedSlot);


    rewardTimer = new QTimer(this);
    connect(rewardTimer, &QTimer::timeout, this, &MainWindow::onRewardTimerTimeoutSlot);

    connectionKeepAliveTimer = new QTimer(this);
    connect(connectionKeepAliveTimer, &QTimer::timeout, this, &MainWindow::onConnectionKeepAliveTimerTimeoutSlot);

    labelTimeUntilOpenKeyTimer = new QTimer(this);
    labelTimeUntilOpenKeyTimer->start(1000);
    connect(labelTimeUntilOpenKeyTimer, &QTimer::timeout, this, &MainWindow::onLabelTimeUntilOpenKeyTimerTimeoutSlot);

    connect(cabinet, &EredanArenaCabinet::signalCollectCard, this, &MainWindow::onCabinetCollectCardSlot);
    connect(cabinet, &EredanArenaCabinet::signalConvertToXp, this, &MainWindow::onCabinetConvertToXpSlot);

    pushButtonOpenKeyEnableTimer = new QTimer(this);
    connect(pushButtonOpenKeyEnableTimer, &QTimer::timeout, this, &MainWindow::onPushButtonOpenKeyEnableTimerTimeoutSlot);

    battle = new EredanArenaBattle(this);
    connect(battle->getTcpSocket(), &QAbstractSocket::disconnected, this, &MainWindow::onBotTcpSocketDisconnectedSlot);
    connect(battle->getTcpSocket(), &QAbstractSocket::connected, this, &MainWindow::onBotTcpSocketConnectedSlot);
    connect(battle, &EredanArenaBattle::signalResultMatch, this, &MainWindow::onBattleResultMatchSlot);
    connect(battle, &EredanArenaBattle::signalUserChoiceCard, this, &MainWindow::onBattleUserChoiceCard);
    connect(battle, &EredanArenaBattle::signalOppChoiceCard, this, &MainWindow::onBattleOppChoiceCard);
    connect(battle, &EredanArenaBattle::signalUserDiceResult, this, &MainWindow::onBattleUserDiceResultSlot);
    connect(battle, &EredanArenaBattle::signalOppDiceResult, this, &MainWindow::onBattleOppDiceResultSlot);
    connect(battle, &EredanArenaBattle::signalResultRound, this, &MainWindow::onBattleResultRoundSlot);
    connect(battle, &EredanArenaBattle::signalPlayDice, this, &MainWindow::onBattlePlayDice);
    connect(battle, &EredanArenaBattle::signalValidDice, this, &MainWindow::onBattleValidDice);

    connect(ui->listWidgetPlayerCards, &QListWidget::itemClicked, this, &MainWindow::onListWidgetPlayerCardsSlotClicked);

    startBotTimer = new QTimer(this);
    connect(startBotTimer, &QTimer::timeout, this, &MainWindow::onBotTimerTimeoutSlot);
    startBotTimer->start(3000);

    site = new ProgramSite(this);
    connect(site, &ProgramSite::signalLCR, this, &MainWindow::onSiteLCRSlot);
    connect(site, &ProgramSite::signalTrialKeyRecieved, this, &MainWindow::onSiteTrialKeyRecievedSlot);

    //error signals
    connect(battle, &EredanArenaBattle::signalError, this, &MainWindow::onErrorSlot);
    connect(cabinet, &EredanArenaCabinet::signalError, this, &MainWindow::onErrorSlot);
    connect(&(Singleton<EredanArenaCards>::instance()), &EredanArenaCards::signalError, this, &MainWindow::onErrorSlot);
    connect(cabinet->getPlayer(), &EredanArenaPlayer::signalError, this, &MainWindow::onErrorSlot);
    connect(&(Singleton<EredanArenaTexts>::instance()), &EredanArenaTexts::signalError, this, &MainWindow::onErrorSlot);
    connect(site, &ProgramSite::signalError, this, &MainWindow::onErrorSlot);

}

MainWindow::~MainWindow()
{
    pushButtonOpenKeyEnableTimer->stop();
    labelTimeUntilOpenKeyTimer->stop();
    connectionKeepAliveTimer->stop();
    rewardTimer->stop();
    startBotTimer->stop();

    delete battle;
    delete pushButtonOpenKeyEnableTimer;
    delete labelTimeUntilOpenKeyTimer;
    delete connectionKeepAliveTimer;
    delete rewardTimer;
    delete startBotTimer;
    delete cabinet;
    delete site;
    delete configs;
    delete ui;
}

 Ui::MainWindow* MainWindow::getUI()
 {
     return ui;
 }

 QString MainWindow::diceListToString(const QList<int> &diceList)
 {
     QString str;

     for(int i = 0; i < diceList.size(); i++)
     {
         if(diceList[i] == 0) str += "R";
         else if(diceList[i] == 1) str += "B";
         else if(diceList[i] == 2) str += "Y";
         else if(diceList[i] == 3) str += "S";
         else str += "?";
     }

     return str;
 }


void MainWindow::onErrorSlot(QString errorMessage)
{
    ui->textEditLog->append(errorMessage);
}

void MainWindow::onCabinetLoginSlot(bool successful)
{
    if(successful)
    {

    }

    ui->textEditLog->append("Trying to auth.");
    cabinet->isLogined();
}

void MainWindow::onIsCabinetLoginedSlot(bool successful)
{
    if(successful)
    {
        ui->textEditLog->append("Auth check: connected.");

        if(isAuthorized == false)
        {
            isAuthorized = true;

            ui->labelAuthStatus->setText("Authorization successful.\nLoading game resources. Please wait.");

            cabinet->loadAccountInfo();

            connectionKeepAliveTimer->start(30000);
        }
    }
    else
    {
        ui->textEditLog->append("Auth check: not connected.");
        ui->pushButtonAuth->setEnabled(true);

        if(isAuthorized)    // connection lost
        {
            // reconnect
            ui->textEditLog->append("Connection with server lost, try reconnect.");
            cabinet->login(ui->lineEditLogin->text(), ui->lineEditPassword->text());
        }
        else
        {
            ui->labelAuthStatus->setText("Can't authorize. Check combination login/password.");
        }
    }
}

void MainWindow::onCabinetAccountInfoLoadedSlot()
{
    qDebug() << "MainWindow::onCabinetAccountInfoLoadedSlot";
    ui->stackedWidget->setCurrentWidget(ui->stackedWidgetPageAccountInfo);

    ui->labelLogin->setText(cabinet->getPlayer()->getPlayerName());
    ui->labelLevel->setText(QString::number(cabinet->getPlayer()->getPlayerLevel()));
    ui->labelCash->setText(QString::number(cabinet->getPlayer()->getPlayerCash()));
    ui->labelKeys->setText(QString::number(cabinet->getPlayer()->getPlayerKeyNum()) + " / " + QString::number(cabinet->getPlayer()->getPlayerKeyMax()));
    ui->labelKeys2->setText(QString::number(cabinet->getPlayer()->getPlayerKeyNum()) + " / " + QString::number(cabinet->getPlayer()->getPlayerKeyMax()));
    ui->labelExp->setText(QString::number(cabinet->getPlayer()->getPlayerXp()) + "/" + QString::number(cabinet->getPlayer()->getPlayerXpMax()));

    ui->labelAuthStatus->setText("Connected.");
    ui->tabKeyOpen->setEnabled(true);
    ui->tabBot->setEnabled(true);

    configs->load(cabinet->getPlayer()->getPlayerName());

    if(configs->getLicensyKey().isEmpty() == false && isLicensyCheckedAfterAuth == false)
    {
        site->checkLicensy(configs->getLicensyKey());
        isLicensyCheckedAfterAuth = true;
    }

}

void MainWindow::onUpdateDataReadProgressSlot(qint64 current, qint64 total)
{
    //ui->progressBarRequests->setValue(current * 100 / total);

    if(current == total)
    {
        ui->labelRequestProcess->setText("");
    }
    else
    {
        ui->labelRequestProcess->setText("Please wait for the end of the request. Recieved bytes: " + QString::number(current));
    }
}

void MainWindow::onCabinetOpenKeySlot(bool successful, QString rewardType, QString rewardId, int status)
{
    QString item;
    item = "[" + QTime::currentTime().toString() + "] ";

    if(successful)
    {

        QString sStatus;

        switch(status)
        {
        case 0:
            sStatus = "new, keep";
            break;

        case 1:
            sStatus = "lvl up 1";
            break;

        case 2:

            if( Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getLevel() == 3)
            {
                sStatus = "already exists, for sale";
            }
            else
            {
                sStatus = "already exists, combine";
            }
            break;

        case 3:
            sStatus = "lvl up";
            break;
        }

        if(rewardType == "booster")
        {
            switch(status)
            {
            case 0:
                if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                    cabinet->collectCard();
            break;

            case 1:
                break;

            case 2:

                if( Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getLevel() == 3)
                {
                    if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                        cabinet->collectCard(); // send sell card
                }
                else
                {
                    if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                        cabinet->convertToXp(); // send combine and win 1
                }
                break;

            case 3:

                if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                    cabinet->collectCard(); // send lvl up
                break;
            }

            item += Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getName() + "[" + QString::number(Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getLevel()) + "] (" +sStatus+ ")";
        }
        else if(rewardType == "card")
        {
            switch(status)
            {
            case 0:
                if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                    cabinet->collectCard();
            break;

            case 1:
                break;

            case 2:

                if( Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getLevel() == 3)
                {
                    if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                        cabinet->collectCard(); // send sell card
                }
                else
                {
                    if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                        cabinet->convertToXp(); // send combine and win 1
                }
                break;

            case 3:

                if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
                    cabinet->collectCard(); // send lvl up
                break;
            }

            item += Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getName() + "[" + QString::number(Singleton<EredanArenaCards>::instance().getCard(rewardId.toInt())->getLevel()) + "] (" +sStatus+ ")";
        }
        else if(rewardType == "cash")
        {
            item += "Cash: " + rewardId + ". ";
            ui->pushButtonOpenKey->setEnabled(true);
        }
        else
        {
            item += "Reward type: " + rewardType + ". ";
            ui->pushButtonOpenKey->setEnabled(true);
        }
    }
    else
    {
        item += "No keys. ";
        ui->pushButtonOpenKey->setEnabled(true);
    }

    ui->listWidgetRewardLog->addItem(item);
}

void MainWindow::onRewardTimerTimeoutSlot()
{
    int delayMinutes = ui->lineEditKeyOpenDelay->text().toInt();
    int delayMilisec = delayMinutes * 60 * 1000;

    if(delayMilisec < 1000)
        delayMilisec = 1000;

    rewardTimer->start(delayMilisec);
    cabinet->openKey();
}

void MainWindow::onConnectionKeepAliveTimerTimeoutSlot()
{
    cabinet->isLogined();
}

void MainWindow::onLabelTimeUntilOpenKeyTimerTimeoutSlot()
{
    int msec = rewardTimer->remainingTime();
    int sec = msec / 1000 % 60;
    int min = msec / 1000 / 60 % 60;
    int hour = msec / 1000 / 60 / 60 % 60;

    ui->labelTimeUntilOpenKey->setText(QString::number(hour) + ":" + QString::number(min) + ":" + QString::number(sec));
}

void MainWindow::onCabinetConvertToXpSlot()
{
    ui->pushButtonOpenKey->setEnabled(true);
}

void MainWindow::onPushButtonOpenKeyEnableTimerTimeoutSlot()
{
    ui->pushButtonOpenKey->setEnabled(true);
}

void MainWindow::onPlayerCardsUpdatedSlot()
{
    ui->listWidgetPlayerCards->clear();

    const QMap <int, EredanArenaCard* >& cards = cabinet->getPlayer()->getPlayerCards();

    foreach(EredanArenaCard* card, cards)
    {
        QListWidgetItem* item = new QListWidgetItem();
        item->setText(card->getName() + " [" + QString::number(card->getLevel()) + "] ");
        item->setData(Qt::UserRole, card->getIdCard());

        if(card->getActive())
        {
            item->setCheckState(Qt::CheckState::Checked);
        }
        else
        {
            item->setCheckState(Qt::CheckState::Unchecked);
        }

        ui->listWidgetPlayerCards->addItem(item);
    }
}

void MainWindow::onListWidgetPlayerCardsSlotClicked(QListWidgetItem *item)
{
    int cardId = item->data(Qt::UserRole).toInt();

}

void MainWindow::onBotTcpSocketDisconnectedSlot()
{
    ui->labelBotStatus->setText("Not runned");

    QList <int> playerCardIds;
    const QMap <int, EredanArenaCard*> playerCards = cabinet->getPlayer()->getPlayerCards();

    foreach(EredanArenaCard* playerCard, playerCards)
    {
        if(playerCard->getActive())
            playerCardIds.push_back(playerCard->getId());
    }

    cabinet->endGame(playerCardIds);
    cabinet->loadOnLoadWelcome(Singleton<EredanArenaCards>::instance().getLoadedCardsSize());

    if(ui->checkBoxClearCurrentFightLog->isChecked())
    {
        ui->listWidgetCurrentFightLog->clear();
    }

    //on_pushButtonBotStart_clicked()
}

void MainWindow::onBotTcpSocketConnectedSlot()
{
    ui->labelBotStatus->setText("Runned");
}

void MainWindow::onBattleResultMatchSlot(int battleResult, const QString &endReason, const QString &userName, const QString &oppName, int scoreUser, int scoreOpp)
{
    QString battleResultString;

    if(battleResult == 0)
        battleResultString = "Win";

    if(battleResult == 1)
        battleResultString = "Lose";

    if(battleResult == 2)
        battleResultString = "Draw";

    QListWidgetItem* item = new QListWidgetItem();
    item->setText("You " + battleResultString + ", " + userName + " vs " + oppName + " (" + QString::number(scoreUser) + ":" + QString::number(scoreOpp) + "). ");
    ui->listWidgetFightsLog->addItem(item);
}

void MainWindow::onBattleUserChoiceCard(EredanArenaCard *card)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Bot choice card: " + card->getName());
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattleOppChoiceCard(EredanArenaCard *card)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Opponent choice card: " + card->getName());
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattleUserDiceResultSlot(const QList<int> &diceList)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Bot dice res: " + diceListToString(diceList));
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattleOppDiceResultSlot(const QList<int> &diceList)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Opponent dice res: " + diceListToString(diceList));
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattleResultRoundSlot(int battleResult, int userScore, int oppScore)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Round score: " + QString::number(userScore) + ":" + QString::number(oppScore));
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattleValidDice(const QList<int> &useDiceList)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Bot dice ok");
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onBattlePlayDice(const QList<int> &useDiceList)
{
    QListWidgetItem* item = new QListWidgetItem();
    item->setText("Bot dice reroll");
    ui->listWidgetCurrentFightLog->addItem(item);
}

void MainWindow::onSiteLCRSlot(const QString &accountName, int currentTime, int licensyEndTime)
{
    qDebug() << "MainWindow::onSiteLCRSlot";

    if(accountName == cabinet->getPlayer()->getPlayerName() && currentTime < licensyEndTime)
    {
        qDebug() << accountName << currentTime << licensyEndTime;
        QDateTime timestamp;
        timestamp.setTime_t(licensyEndTime);

        ui->stackedWidgetLicensy->setCurrentWidget(ui->stackedWidgetPageLicensyFull);
        ui->labelLicensyEndTime->setText(timestamp.toString(Qt::SystemLocaleShortDate));
    }
    else
    {
        ui->stackedWidgetLicensy->setCurrentWidget(ui->stackedWidgetPageLicensyInvalid);
    }
}

void MainWindow::onSiteTrialKeyRecievedSlot(const QString &key)
{
    qDebug() << "MainWindow::onSiteTrialKeyRecievedSlot";
    ui->lineEditLicensyKey->setText(key);
    QTimer::singleShot(500, this, &MainWindow::onStartDelayedLCRSlot);
}

void MainWindow::onStartDelayedLCRSlot()
{
    if(ui->lineEditLicensyKey->text().isEmpty() == false)
    {
        QString licensyKey = ui->lineEditLicensyKey->text();
        configs->setLicensyKey(licensyKey);
        site->checkLicensy(configs->getLicensyKey());
    }
}

void MainWindow::onStartDelyedLicensyKeyInfoSlot()
{
    QDesktopServices::openUrl(QUrl(site->getUrlSite() + "licensy-key-info.php?key=" + configs->getLicensyKey()));
}

void MainWindow::onStartDelyedLTASlot()
{
    site->activateLicensyTrial(cabinet->getPlayer()->getPlayerName());
}

void MainWindow::onBotTimerTimeoutSlot()
{
    if(battle->getTcpSocket()->state() == QAbstractSocket::SocketState::UnconnectedState)
    {
        if(isBotStarted)
            on_pushButtonBotStart_clicked();
    }

    //qDebug() << battle->getTcpSocket()->state();

    startBotTimer->start(3000);
}

void MainWindow::onCabinetCollectCardSlot()
{
    ui->pushButtonOpenKey->setEnabled(true);
}

void MainWindow::on_pushButtonAuth_clicked()
{
    cabinet->login(ui->lineEditLogin->text(), ui->lineEditPassword->text());

    ui->pushButtonAuth->setEnabled(false);
}

void MainWindow::on_pushButtonDisconnect_clicked()
{
    // stop timers
    rewardTimer->stop();
    connectionKeepAliveTimer->stop();

    isAuthorized = false;
    cabinet->logout();

    isLicensyCheckedAfterAuth = false;
    configs->save();

    // gui
    ui->stackedWidget->setCurrentWidget(ui->stackedWidgetPageAccount);
    ui->stackedWidgetLicensy->setCurrentWidget(ui->stackedWidgetPageLicensyUnregistered);
    ui->pushButtonAuth->setEnabled(true);
    ui->labelAuthStatus->setText("Not authorized.");
    ui->tabKeyOpen->setEnabled(false);
    ui->tabBot->setEnabled(false);
}

void MainWindow::on_pushButtonOpenKey_clicked()
{
    if(ui->checkBoxAutoKeepCombineSellCard->checkState() == Qt::CheckState::Checked)
    {
        ui->pushButtonOpenKey->setEnabled(false);
        pushButtonOpenKeyEnableTimer->start(5000);
    }

    cabinet->openKey();
}

void MainWindow::on_pushButtonStartStopAutoKeyOpen_clicked()
{
    if(rewardTimer->isActive()) // Stop auto open keys
    {
        ui->pushButtonStartStopAutoKeyOpen->setText("Start");
        rewardTimer->stop();
    }
    else    // Start auto open keys
    {
        ui->pushButtonStartStopAutoKeyOpen->setText("Stop");

        int delayMinutes = ui->lineEditKeyOpenDelay->text().toInt();
        int delayMilisec = delayMinutes * 60 * 1000;

        if(delayMilisec < 5000)
            delayMilisec = 5000;

        rewardTimer->start(delayMilisec);
        cabinet->openKey();
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->textEditLog->clear();
}

void MainWindow::on_pushButtonBotStart_clicked()
{
    battle->start(EredanArenaBattle::Type::Training, cabinet->getPlayer()->getPlayerToken());
    isBotStarted = true;
}

void MainWindow::on_pushButtonBotStop_clicked()
{
    if(isBotStarted)
    {
        isBotStarted = false;
        ui->labelBotStatus->setText("Waiting end fight");
    }
}


void MainWindow::on_pushButtonBuyLicensy_clicked()
{
    QDesktopServices::openUrl(QUrl(site->getUrlSite() + "licensy-keys.php"));
}

void MainWindow::on_pushButtonLicensyActivate_clicked()
{
    QTimer::singleShot(500, this, &MainWindow::onStartDelayedLCRSlot);
}

void MainWindow::on_pushButtonBackToLicensyUnregistered_clicked()
{
    ui->stackedWidgetLicensy->setCurrentWidget(ui->stackedWidgetPageLicensyUnregistered);
}

void MainWindow::on_pushButtonLicensyKeyInfo_clicked()
{
    QTimer::singleShot(500, this, &MainWindow::onStartDelyedLicensyKeyInfoSlot);
}

void MainWindow::on_pushButtonLicensyTrialActivate_clicked()
{
    QTimer::singleShot(500, this, &MainWindow::onStartDelyedLTASlot);
}
