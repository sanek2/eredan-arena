#include <QDebug>
#include "programconfigs.h"

ProgramConfigs::ProgramConfigs(QObject *parent) : QObject(parent)
{

}

ProgramConfigs::~ProgramConfigs()
{
    save();
}

QString ProgramConfigs::getFileName() const
{
    return fileName;
}

void ProgramConfigs::setFileName(const QString &value)
{
    fileName = value;
}

QString ProgramConfigs::getDirPath()
{
    QString dirPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(dirPath);

    if(!dir.exists())
    {
        dir.mkpath(dirPath);
    }

    return dirPath;
}

QString ProgramConfigs::getFilePath()
{
    return getDirPath() + "/" + getFileName();
}

QString ProgramConfigs::getLicensyKey() const
{
    return licensyKey;
}

void ProgramConfigs::setLicensyKey(const QString &value)
{
    licensyKey = value;
}

bool ProgramConfigs::load(const QString& playerName)
{
    setPlayerName(playerName);

    QFile loadFile(getFilePath());

    if (!loadFile.open(QIODevice::ReadOnly))
    {
        qWarning("Couldn't open read file.");
        return false;
    }

    QByteArray readData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(readData));
    QJsonObject jsonObject = loadDoc.object();

    if(jsonObject["accounts"].isUndefined() == false && jsonObject["accounts"].isNull() == false)
    {
        QJsonObject jsonObjectAccounts = jsonObject["accounts"].toObject();

        if(getPlayerName().isEmpty() == false && jsonObjectAccounts[getPlayerName()].isUndefined() == false && jsonObjectAccounts[getPlayerName()].isNull() == false)
        {
            QJsonObject jsonObjectAccount = jsonObjectAccounts[getPlayerName()].toObject();
            setLicensyKey(jsonObjectAccount["licensyKey"].toVariant().toString());
        }
    }

    loadFile.close();

    return true;
}

bool ProgramConfigs::save()
{
    QFile loadFile(getFilePath());

    QByteArray readData;

    if (loadFile.open(QIODevice::ReadOnly))
    {
        readData = loadFile.readAll();
        //qDebug() << "readData: " << readData;
    }
    else
    {
        qWarning("Couldn't open load (2) file.");
    }

    loadFile.close();

    QFile saveFile(getFilePath());

    if (!saveFile.open(QIODevice::WriteOnly))
    {
        qWarning("Couldn't open save file.");
        return false;
    }

    QJsonDocument loadDoc(QJsonDocument::fromJson(readData));
    QJsonObject jsonObject = loadDoc.object();

    QJsonObject jsonObjectAccounts = jsonObject["accounts"].toObject();

    if(jsonObjectAccounts.isEmpty())
    {
        jsonObjectAccounts = QJsonObject();
    }

    if(getPlayerName().isEmpty() == false)
    {
        QJsonObject jsonObjectAccount;
        jsonObjectAccount["licensyKey"] = getLicensyKey();

        jsonObjectAccounts[getPlayerName()] = jsonObjectAccount;
    }

    jsonObject["accounts"] = jsonObjectAccounts;

    QJsonDocument saveDoc(jsonObject);
    //qDebug() << getPlayerName() << getLicensyKey() << jsonObjectAccounts << jsonObject["accounts"].toObject() << saveDoc.toJson();
    saveFile.write(saveDoc.toJson());
    saveFile.close();

    return true;
}

QString ProgramConfigs::getPlayerName() const
{
    return playerName;
}

void ProgramConfigs::setPlayerName(const QString &value)
{
    playerName = value;
}

