#ifndef EREDANARENACARDS_H
#define EREDANARENACARDS_H

#include <QObject>
#include <QString>
#include <QJsonObject>
#include "eredanarenacard.h"

#ifndef MAX_CARD_ID
    #define MAX_CARD_ID 5000
#endif

class EredanArenaCards : public QObject
{
    Q_OBJECT
public:
    explicit EredanArenaCards(QObject *parent = 0);
    ~EredanArenaCards();
    EredanArenaCard* getCard(int id);
    int getLoadedCardsSize();
    bool issetCard(int id);
    void parseJsonCards(const QJsonObject& jsonCards);
    void clear();

signals:
    void signalError(QString message);

public slots:

private:
    EredanArenaCard* cards[MAX_CARD_ID];            // array of all cards
};

#endif // EREDANARENACARDS_H
