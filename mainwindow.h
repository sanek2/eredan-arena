#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QString>
#include <QList>
#include <QTimer>
#include <QListWidgetItem>
#include "eredanarenacabinet.h"
#include "eredanarenabattle.h"
#include "programsite.h"
#include "programconfigs.h"


namespace Ui {
class MainWindow;
}

QT_BEGIN_NAMESPACE
class QNetworkReply;

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow* getUI();

    QString diceListToString(const QList <int>& diceList);


private slots:
    //void httpFinished();
    void onCabinetLoginSlot(bool successful);
    void onIsCabinetLoginedSlot(bool successful);
    void onErrorSlot(QString errorMessage);
    void onCabinetAccountInfoLoadedSlot();
    void onUpdateDataReadProgressSlot(qint64 current, qint64 total);
    void onCabinetOpenKeySlot(bool successful, QString rewardType, QString rewardId, int status);
    void onRewardTimerTimeoutSlot();
    void onConnectionKeepAliveTimerTimeoutSlot();
    void onLabelTimeUntilOpenKeyTimerTimeoutSlot();
    void onCabinetCollectCardSlot();
    void onCabinetConvertToXpSlot();
    void onPushButtonOpenKeyEnableTimerTimeoutSlot();
    void onPlayerCardsUpdatedSlot();
    void onListWidgetPlayerCardsSlotClicked(QListWidgetItem *item);
    void onBotTcpSocketDisconnectedSlot();
    void onBotTcpSocketConnectedSlot();
    void onBattleResultMatchSlot(int battleResult, const QString& endReason, const QString& userName, const QString& oppName, int scoreUser, int scoreOpp);
    void onBattleUserChoiceCard(EredanArenaCard* card);
    void onBattleOppChoiceCard(EredanArenaCard* card);
    void onBattleUserDiceResultSlot(const QList<int>& diceList);
    void onBattleOppDiceResultSlot(const QList<int>& diceList);
    void onBattleResultRoundSlot(int battleResult, int userScore, int oppScore);
    void onBattleValidDice(const QList<int>& useDiceList);
    void onBattlePlayDice(const QList<int>& useDiceList);
    void onSiteLCRSlot(const QString& accountName, int currentTime, int licensyEndTime);
    void onSiteTrialKeyRecievedSlot(const QString& key);
    void onStartDelayedLCRSlot();
    void onStartDelyedLicensyKeyInfoSlot();
    void onStartDelyedLTASlot();    // delayed licensy trial activation

    void onBotTimerTimeoutSlot();

    void on_pushButtonAuth_clicked();

    void on_pushButtonDisconnect_clicked();

    void on_pushButtonOpenKey_clicked();

    void on_pushButtonStartStopAutoKeyOpen_clicked();

    void on_pushButton_3_clicked();

    void on_pushButtonBotStart_clicked();

    void on_pushButtonBotStop_clicked();

    void on_pushButtonBuyLicensy_clicked();

    void on_pushButtonLicensyActivate_clicked();

    void on_pushButtonBackToLicensyUnregistered_clicked();

    void on_pushButtonLicensyKeyInfo_clicked();

    void on_pushButtonLicensyTrialActivate_clicked();

private:
    Ui::MainWindow* ui;
    EredanArenaCabinet* cabinet;
    QNetworkAccessManager qnam;
    QNetworkReply* reply;
    QTimer* rewardTimer;
    QTimer* connectionKeepAliveTimer;
    QTimer* labelTimeUntilOpenKeyTimer;
    QTimer* pushButtonOpenKeyEnableTimer;
    QTimer* startBotTimer;
    EredanArenaBattle* battle;
    ProgramSite* site;
    ProgramConfigs* configs;

    bool isAuthorized = false;
    bool isBotStarted = false;
    bool isLicensyCheckedAfterAuth = false;
};

#endif // MAINWINDOW_H
