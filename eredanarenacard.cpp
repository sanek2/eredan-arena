#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QVariant>
#include "eredanarenacard.h"
#include "singleton.h"
#include "eredanarenatexts.h"


EredanArenaCard::EredanArenaCard(QObject *parent) : QObject(parent)
{

}

void EredanArenaCard::parseJsonCardInfo(QJsonObject& jsonCard)
{
    setIdCard(jsonCard["id"].toVariant().toInt());
    setLevel(jsonCard["niveau"].toVariant().toInt());
    setIdTextName(jsonCard["id_nom"].toVariant().toInt());

    if(jsonCard["exclu"].isUndefined() == false && jsonCard["exclu"].isNull() == false)
        setExclusive(jsonCard["exclu"].toVariant().toInt());

    if(jsonCard["capacites"].isUndefined() == false && jsonCard["capacites"].isNull() == false)
    {
        cardSkills.clear();

        foreach(auto &jsonSkill, jsonCard["capacites"].toArray())
        {
            QJsonObject jsonObjectSkill = jsonSkill.toObject();
            EredanArenaCardSkill* skill = new EredanArenaCardSkill(this);
            skill->parseJsonCardSkill(jsonObjectSkill);
            cardSkills.push_back(skill);
        }
    }
}

void EredanArenaCard::parseJsonPlayerCardInfo(QJsonObject& jsonPlayerCard)
{
    setId(jsonPlayerCard["id"].toVariant().toInt());
    setIdCard(jsonPlayerCard["id_carte"].toVariant().toInt());
    setIdPlayer(jsonPlayerCard["id_joueur"].toVariant().toInt());
    setXp(jsonPlayerCard["xp"].toVariant().toInt());
    setTypeCard(jsonPlayerCard["type_carde"].toVariant().toInt());
    setActive(jsonPlayerCard["active"].toVariant().toInt() == 1);
}

bool EredanArenaCard::getActive() const
{
    return active;
}

void EredanArenaCard::setActive(bool value)
{
    active = value;
}

int EredanArenaCard::getId() const
{
    return id;
}

void EredanArenaCard::setId(int value)
{
    id = value;
}

int EredanArenaCard::getIdCard() const
{
    return idCard;
}

void EredanArenaCard::setIdCard(int value)
{
    idCard = value;
}

int EredanArenaCard::getIdPlayer() const
{
    return idPlayer;
}

void EredanArenaCard::setIdPlayer(int value)
{
    idPlayer = value;
}

int EredanArenaCard::getXp() const
{
    return xp;
}

void EredanArenaCard::setXp(int value)
{
    xp = value;
}

int EredanArenaCard::getTypeCard() const
{
    return typeCard;
}

void EredanArenaCard::setTypeCard(int value)
{
    typeCard = value;
}

int EredanArenaCard::getExclusive() const
{
    return exclusive;
}

void EredanArenaCard::setExclusive(int value)
{
    exclusive = value;
}

int EredanArenaCard::getLevel() const
{
    return level;
}

void EredanArenaCard::setLevel(int value)
{
    level = value;
}

int EredanArenaCard::getIdTextName() const
{
    return idTextName;
}

void EredanArenaCard::setIdTextName(int value)
{
    idTextName = value;
}

QString EredanArenaCard::getName()
{
    return Singleton<EredanArenaTexts>::instance().getText(getIdTextName());
}

QList<EredanArenaCardSkill *> EredanArenaCard::getCardSkills() const
{
    return cardSkills;
}

void EredanArenaCard::setCardSkills(const QList<EredanArenaCardSkill *> &value)
{
    cardSkills = value;
}
