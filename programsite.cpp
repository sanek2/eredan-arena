#include <QList>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QTimer>
#include "programsite.h"
#include "eredanarenacabinet.h"

ProgramSite::ProgramSite(QObject *parent) : QObject(parent)
{
    qnam.setCookieJar(new QNetworkCookieJar);
    connect(&qnam, SIGNAL(finished(QNetworkReply*)), this, SLOT(finishedRequestSlot(QNetworkReply*)));

    blowfish = new QBlowfish("12345678");   // %6s_9i^2aBs$'

    //blowfish->calcSubKey("Or lets set another key");
    //QByteArray encryptedData = blowfish->encrypt("");
    //QByteArray decryptedData = blowfish->decrypt(encryptedData);


}

ProgramSite::~ProgramSite()
{
    delete blowfish;
}

void ProgramSite::checkLicensy(const QString &licensyKey)
{
    QUrl url = urlSite + "licensy.php";
    QString params = licensyKey;

    QUrlQuery postData;
    postData.addQueryItem("key", params);

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::CheckLicensy);
    //connect(reply, &QNetworkReply::downloadProgress, this, &EredanArenaCabinet::updateDataReadProgressSlot);
}

void ProgramSite::activateLicensyTrial(const QString &accountName)
{
    QUrl url = urlSite + "licensy-trial.php";
    QString params = accountName;

    QUrlQuery postData;
    postData.addQueryItem("key", params);

    QNetworkRequest request = QNetworkRequest(url);

    request.setRawHeader("Origin", "http://www.eredan-arena.com");
    request.setRawHeader("X-Requested-With", "ShockwaveFlash/22.0.0.209");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("Referer", "http://www.eredan-arena.com/EredanArenaWebPreloader_1470397606.swf");
    request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    request.setRawHeader("Connection", "close");

    QNetworkReply* reply = qnam.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    reply->setProperty("requestType", RequestType::ActivateLicensyTrial);
}

void ProgramSite::onLCR()
{
    qDebug() << "ProgramSite::onLCR";

    if(accountName.length() && currentTime && licensyEndTime)
    {
        emit signalLCR(accountName, currentTime, licensyEndTime);
    }

    accountName = "";
    currentTime = 0;
    licensyEndTime = 0;
}

void ProgramSite::finishedRequestSlot(QNetworkReply *reply)
{
    if (reply->error())
    {
        emit signalError("HTTP error. Code: " + reply->error());
    }
    else
    {
        QList<QNetworkCookie> cookies = qvariant_cast<QList<QNetworkCookie>>(reply->header(QNetworkRequest::SetCookieHeader));

        if(cookies.count() != 0)
        {
            //you must tell which cookie goes with which url
            qnam.cookieJar()->setCookiesFromUrl(cookies, reply->url());
        }

        QByteArray data = reply->readAll();
        handleRequest(data, reply->property("requestType").toInt());
    }

    reply->deleteLater();
}

QString ProgramSite::getUrlSite() const
{
    return urlSite;
}


void ProgramSite::handleRequest(const QByteArray &data, int type)
{
    QJsonDocument jsonDoc;
    QJsonParseError jsonParseError;
    QString jsonStr;
    QJsonObject jsonObject;
    QJsonArray jsonArray;

    switch(type)
    {
    case RequestType::CheckLicensy:

        jsonStr = blowfish->decrypted(QByteArray::fromBase64(data));
        qDebug() << "RequestType::CheckLicensy" << jsonStr;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else
        {
            if(jsonObject["account"].isUndefined() == false && jsonObject["account"].isNull() == false)
            {
                accountName = jsonObject["account"].toVariant().toString();
            }

            if(jsonObject["licensyEndTime"].isUndefined() == false && jsonObject["licensyEndTime"].isNull() == false)
            {
                licensyEndTime = jsonObject["licensyEndTime"].toVariant().toInt();
            }

            if(jsonObject["currentTime"].isUndefined() == false && jsonObject["currentTime"].isNull() == false)
            {
                currentTime = jsonObject["currentTime"].toVariant().toInt();
            }

            QTimer::singleShot(500, this, &ProgramSite::onLCR);

        }
        break;

    case RequestType::ActivateLicensyTrial:
        jsonStr = blowfish->decrypted(QByteArray::fromBase64(data));
        qDebug() << "RequestType::ActivateLicensyTrial" << jsonStr;

        jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &jsonParseError);
        jsonObject = jsonDoc.object();

        //qDebug() << jsonStr;
        //qDebug() << jsonObject;

        if(jsonStr.length() == 0)
        {
            signalError("Empty answer from server.");
        }
        else if (jsonParseError.error != QJsonParseError::NoError)
        {
            QString err = "Parse json answer error. Json: \"" + jsonStr + "\" Type error: \" " +jsonParseError.errorString()+ " \" ";
            signalError(err);
        }
        else
        {
            QString key;

            if(jsonObject["key"].isUndefined() == false && jsonObject["key"].isNull() == false)
            {
                key = jsonObject["key"].toVariant().toString();
                signalTrialKeyRecieved(key);
            }
        }

        break;

    default:
        emit signalError("Undefined request type " + QString::number(type));
        break;
    }
}

