#include <QVariant>
#include <QString>
#include <QDebug>
#include "eredanarenatexts.h"

EredanArenaTexts::EredanArenaTexts(QObject *parent) : QObject(parent)
{

}

QString &EredanArenaTexts::getText(int id)
{
    if(id < MAX_TEXT_ID && id >= 0)
    {
        return texts[id];
    }
    else
    {
        return texts[MAX_TEXT_ID - 1];
        emit signalError("Parse error. Invalid text id: " + QString::number(id) + ".");
    }
}

bool EredanArenaTexts::issetText(int id)
{
    if(id < MAX_TEXT_ID && id >= 0)
    {
        return (texts[id].length() > 0);
    }

    return false;
}

void EredanArenaTexts::parseJsonTexts(const QJsonObject &jsonTexts)
{
    for(QJsonObject::const_iterator it = jsonTexts.begin(); it != jsonTexts.end(); it++)
    {
        int textId = it.key().toInt();
        QJsonValue jsonValueText = it.value();

        QString& text = getText(textId);
        text = jsonValueText.toVariant().toString();
    }
}

