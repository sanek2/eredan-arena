#include <QDebug>
#include "eredanarenaplayer.h"

EredanArenaPlayer::EredanArenaPlayer(QObject *parent) : QObject(parent)
{

}

void EredanArenaPlayer::parseJsonEnergyInfo(const QJsonObject &jsonKeyInfo)
{
    if(jsonKeyInfo["pe"].isUndefined() == false && jsonKeyInfo["pe"].isNull() == false)
        playerKeyNum = jsonKeyInfo["pe"].toVariant().toInt();

    if(jsonKeyInfo["pe_max"].isUndefined() == false && jsonKeyInfo["pe_max"].isNull() == false)
        playerKeyMax = jsonKeyInfo["pe_max"].toVariant().toInt();

    if(jsonKeyInfo["pe_regen"].isUndefined() == false && jsonKeyInfo["pe_regen"].isNull() == false)
        playerKeyRegen = jsonKeyInfo["pe_regen"].toVariant().toInt();
}

void EredanArenaPlayer::parseJsonPlayerInfo(const QJsonObject &jsonPlayer)
{
    playerCash = jsonPlayer["cash"].toVariant().toInt();
    playerName = jsonPlayer["pseudo"].toVariant().toString();
    playerId = jsonPlayer["id"].toVariant().toInt();
    playerToken = jsonPlayer["token"].toVariant().toString();

    QJsonObject jsonKeyInfo = jsonPlayer["energyInfos"].toObject();
    parseJsonEnergyInfo(jsonKeyInfo);

    QJsonObject jsonXpInfo = jsonPlayer["xpInfos"].toObject();
    playerLevel = jsonXpInfo["niveau"].toVariant().toInt();
    playerXp = jsonXpInfo["xp"].toVariant().toInt();
    playerXpMax = jsonXpInfo["xp_max"].toVariant().toInt();
    playerXpMin = jsonXpInfo["xp_min"].toVariant().toInt();

    //                qDebug() << playerCash;
    //                qDebug() << playerName;
    //                qDebug() << playerId;
    //                qDebug() << playerToken;

    //                qDebug() << playerKeyNum;
    //                qDebug() << playerKeyMax;
    //                qDebug() << playerKeyRegen;

    //                qDebug() << playerLevel;
    //                qDebug() << playerXp;
    //                qDebug() << playerXpMax;
    //                qDebug() << playerXpMin;
}

int EredanArenaPlayer::getPlayerId() const
{
    return playerId;
}

void EredanArenaPlayer::setPlayerId(int value)
{
    playerId = value;
}

int EredanArenaPlayer::getPlayerCash() const
{
    return playerCash;
}

void EredanArenaPlayer::setPlayerCash(int value)
{
    playerCash = value;
}

int EredanArenaPlayer::getPlayerCrystal() const
{
    return playerCrystal;
}

void EredanArenaPlayer::setPlayerCrystal(int value)
{
    playerCrystal = value;
}

QString EredanArenaPlayer::getPlayerName() const
{
    return playerName;
}

void EredanArenaPlayer::setPlayerName(const QString &value)
{
    playerName = value;
}

QString EredanArenaPlayer::getPlayerToken() const
{
    return playerToken;
}

void EredanArenaPlayer::setPlayerToken(const QString &value)
{
    playerToken = value;
}

int EredanArenaPlayer::getPlayerKeyNum() const
{
    return playerKeyNum;
}

void EredanArenaPlayer::setPlayerKeyNum(int value)
{
    playerKeyNum = value;
}

int EredanArenaPlayer::getPlayerKeyMax() const
{
    return playerKeyMax;
}

void EredanArenaPlayer::setPlayerKeyMax(int value)
{
    playerKeyMax = value;
}

int EredanArenaPlayer::getPlayerKeyRegen() const
{
    return playerKeyRegen;
}

void EredanArenaPlayer::setPlayerKeyRegen(int value)
{
    playerKeyRegen = value;
}

int EredanArenaPlayer::getPlayerLevel() const
{
    return playerLevel;
}

void EredanArenaPlayer::setPlayerLevel(int value)
{
    playerLevel = value;
}

int EredanArenaPlayer::getPlayerXp() const
{
    return playerXp;
}

void EredanArenaPlayer::setPlayerXp(int value)
{
    playerXp = value;
}

int EredanArenaPlayer::getPlayerXpMax() const
{
    return playerXpMax;
}

void EredanArenaPlayer::setPlayerXpMax(int value)
{
    playerXpMax = value;
}

int EredanArenaPlayer::getPlayerXpMin() const
{
    return playerXpMin;
}

void EredanArenaPlayer::setPlayerXpMin(int value)
{
    playerXpMin = value;
}

EredanArenaCard *EredanArenaPlayer::getPlayerCard(int id)
{
    auto it = playerCards.find(id);

    if(it == playerCards.end())
    {
        playerCards[id] = new EredanArenaCard(this);
    }

    return playerCards[id];
}

const QMap<int, EredanArenaCard *> &EredanArenaPlayer::getPlayerCards()
{
    return playerCards;
}

EredanArenaCard* EredanArenaPlayer::loadPlayerCardByCard(const EredanArenaCard *card)
{
    EredanArenaCard* playerCard = getPlayerCard(card->getIdCard());

    playerCard->setIdCard(card->getIdCard());
    playerCard->setLevel(card->getLevel());
    playerCard->setIdTextName(card->getIdTextName());
    playerCard->setExclusive(card->getExclusive());

    playerCard->setCardSkills(card->getCardSkills());

    return playerCard;
}

void EredanArenaPlayer::deletePlayerCard(int id)
{
    auto it = playerCards.find(id);

    if(it != playerCards.end())
        playerCards.erase(it);
}

void EredanArenaPlayer::clearPlayerCards()
{
    foreach(auto &card, playerCards)
    {
        card->deleteLater();
    }

    playerCards.clear();
}

void EredanArenaPlayer::parseJsonPlayerCards(const QJsonObject &jsonCards)
{
    foreach (const QJsonValue& jsonCard, jsonCards)
    {
        QJsonObject jsonObjectCard = jsonCard.toObject();

        int cardId = jsonObjectCard["id_carte"].toVariant().toInt();

        EredanArenaCard* card = Singleton<EredanArenaCards>::instance().getCard(cardId);
        EredanArenaCard *playerCard = loadPlayerCardByCard(card);
        playerCard->parseJsonPlayerCardInfo(jsonObjectCard);

    }

    emit signalPlayerCardsUpdated();
}

