#ifndef EREDANARENABATTLE_H
#define EREDANARENABATTLE_H

#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QDomNode>
#include <QDomNodeList>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include "singleton.h"
#include "eredanarenacards.h"
#include "eredanarenacard.h"

class EredanArenaBattle : public QObject
{
    Q_OBJECT
public:

    enum Type
    {
        Undefined,
        Training,
        Survival,
        League
    };

    enum AnimationList
    {
        resultRound,
        resultMatch,
        score,
        cardDamage,
        cardEffectActivate,
        cardForceBonus,
        cardForceChange,
        cardHeal,
        diceSwitch,
        removeCard,
        cardShield,
        diceAdd,
        diceRemove,
        cardBlock,
        cardInBench,
        cardEffectBuff,
        beginFight, // dice_result
        cardAlterationAdd,
        cardAlterationSet,
        cardEsquive,
        cardEffectReset,
        cardEffectEnd,
        cardDmgGriffe,
        cardEffectMiss,
        cardGriffeActivate,
        doNothing,
        setRelance,
        cardFinishHim
    };

    explicit EredanArenaBattle(QObject *parent = 0);
    ~EredanArenaBattle();

    void start(int type = Type::Undefined, QString token = "");

    int getType() const;
    void setType(int value);

    QString getToken() const;
    void setToken(const QString &value);

    int getRoomId() const;
    void setRoomId(int value);

    int getRoundNum() const;
    void setRoundNum(int value);

    int getIsPve() const;
    void setIsPve(int value);

    int getIsAttacker() const;
    void setIsAttacker(int value);

    QTcpSocket *getTcpSocket() const;
    void setTcpSocket(QTcpSocket *value);

signals:
    void signalError(const QString& error);
    void signalResultMatch(int battleResult, const QString& endReason, const QString& userName, const QString& oppName, int scoreUser, int scoreOpp);
    void signalUserChoiceCard(EredanArenaCard* card);
    void signalOppChoiceCard(EredanArenaCard* card);
    void signalUserDiceResult(const QList<int>& diceList);
    void signalPlayDice(const QList<int>& useDiceList);
    void signalValidDice(const QList<int>& useDiceList);
    void signalOppDiceResult(const QList<int>& diceList);
    void signalResultRound(int value, int userScore, int oppScore);

private slots:
    void readTcpData();
    void tcpErrorSlot(QAbstractSocket::SocketError);

private:
    int type = Type::Training;
    QString token;
    int roomId = 0;
    bool battleEnded = true;
    QTcpSocket* tcpSocket;
    QByteArray buffer;
    int roundNum = 0;
    int isPve = 0;
    int isAttacker = 0;
    QList<EredanArenaCard*>userCardList;
    QList<EredanArenaCard*>oppCardList;
    QList<int> lastUserDiceList;
    EredanArenaCard* lastUserCard = 0;

    void handleSocketConnection();
    void handleMessage(const QByteArray &sPacket);
    void xmlReceived(const QByteArray &sPacket);
    void handleMessageSys(const QDomElement &messageElement);
    void strReceived(const QByteArray &sPacket);
    void handleMessageXt(QList<QByteArray> &parts);
    void jsonReceived(const QByteArray &sPacket);
    void handleMessageXt(const QJsonObject &jsonObjectParam1);
    void onExtensionResponse(const QJsonObject &_loc8_, const QJsonObject &_loc15_, QString _loc16_);
    void dispatchCommandToDelegates(const QString &param2, const QJsonObject &param3);
    void sendPlayCard(int cardNumber);
    void sendPlayDice(const QList<int>& useDiceList);
    void sendValidDice(const QList<int>& useDiceList);
    void onDiceResult(const QList <int>& diceList, int numRetry);

    void parseJsonResultMatch(QJsonObject& jsonObject);
    void parseJsonBeginFight(QJsonObject& jsonObject);
    void parseJsonResultRound(QJsonObject& jsonObject);

    void updateLastUserDiceList(const QList<int>& diceList);

};

#endif // EREDANARENABATTLE_H
