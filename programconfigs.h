#ifndef PROGRAMCONFIGS_H
#define PROGRAMCONFIGS_H

#include <QByteArray>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QStandardPaths>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>

class ProgramConfigs : public QObject
{
    Q_OBJECT
public:
    explicit ProgramConfigs(QObject *parent = 0);
    ~ProgramConfigs();

    QString getFileName() const;
    void setFileName(const QString &value);

    QString getDirPath();
    QString getFilePath();

    QString getLicensyKey() const;
    void setLicensyKey(const QString &value);

    bool load(const QString& playerName);
    bool save();

    QString getPlayerName() const;
    void setPlayerName(const QString &value);

signals:

public slots:

private:
    QString fileName = "configs.dat";
    QString licensyKey;
    QString playerName;
};

#endif // PROGRAMCONFIGS_H
