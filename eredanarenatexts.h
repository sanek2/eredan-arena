#ifndef EREDANARENATEXTS_H
#define EREDANARENATEXTS_H

#include <QObject>
#include <QString>
#include <QJsonObject>

#ifndef MAX_TEXT_ID
    #define MAX_TEXT_ID 10000
#endif

class EredanArenaTexts : public QObject
{
    Q_OBJECT
public:
    explicit EredanArenaTexts(QObject *parent = 0);
    QString& getText(int id);
    bool issetText(int id);
    void parseJsonTexts(const QJsonObject& jsonTexts);

signals:
    void signalError(QString message);

public slots:

private:
    QString texts[MAX_TEXT_ID];
};

#endif // EREDANARENATEXTS_H
