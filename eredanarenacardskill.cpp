#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QVariant>
#include <QDebug>
#include "eredanarenacardskill.h"

EredanArenaCardSkill::EredanArenaCardSkill(QObject *parent) : QObject(parent)
{

}

void EredanArenaCardSkill::parseJsonCardSkill(const QJsonObject &jsonObjectSkill)
{
    setCardId(jsonObjectSkill["id_carte"].toVariant().toInt());
    setEffectId(jsonObjectSkill["effet"].toVariant().toInt());
    setId(jsonObjectSkill["id"].toVariant().toInt());
    setValue(jsonObjectSkill["valeur"].toVariant().toInt());
    setIdEffectName(jsonObjectSkill["script_effet"].toObject()["id_nom"].toVariant().toInt());
    setIdEffectDescription(jsonObjectSkill["script_effet"].toObject()["id_description"].toVariant().toInt());

    QList <int> activationList;

//    qDebug() << "cardSkill";

    foreach(auto& activationJson, jsonObjectSkill["activation"].toArray())
    {
        int dice = activationJson.toVariant().toInt();
//        qDebug() << dice;
        activationList.push_back(dice);
    }

//    qDebug() << getCardId();
//    qDebug() << getEffectId();
//    qDebug() << getId();
//    qDebug() << getValue();
//    qDebug() << getIdEffectName();
//    qDebug() << getIdEffectDescription();

    setActivation(activationList);
}

int EredanArenaCardSkill::getEffectId() const
{
    return effectId;
}

void EredanArenaCardSkill::setEffectId(int value)
{
    effectId = value;
}

int EredanArenaCardSkill::getId() const
{
    return id;
}

void EredanArenaCardSkill::setId(int value)
{
    id = value;
}

int EredanArenaCardSkill::getCardId() const
{
    return cardId;
}

void EredanArenaCardSkill::setCardId(int value)
{
    cardId = value;
}

int EredanArenaCardSkill::getValue() const
{
    return val;
}

void EredanArenaCardSkill::setValue(int value)
{
    val = value;
}

QList<int> EredanArenaCardSkill::getActivation() const
{
    return activation;
}

void EredanArenaCardSkill::setActivation(const QList<int> &value)
{
    activation = value;
}

int EredanArenaCardSkill::getIdEffectName() const
{
    return idEffectName;
}

void EredanArenaCardSkill::setIdEffectName(int value)
{
    idEffectName = value;
}

int EredanArenaCardSkill::getIdEffectDescription() const
{
    return idEffectDescription;
}

void EredanArenaCardSkill::setIdEffectDescription(int value)
{
    idEffectDescription = value;
}

